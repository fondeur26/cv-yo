<?php
//1 Les données obligatoires sont elles pr�sentes ?
if (!empty($_POST)) { 

if( isset( $_POST["prenom"]) && isset( $_POST["nom"]) && isset( $_POST["email"]) && isset( $_POST["message"]) && isset( $_POST["telephone"])){
 
    //2 Les données obligatoires sont-elles remplies ?
    if( !empty( $_POST["prenom"]) && !empty( $_POST["nom"]) && !empty( $_POST["email"]) && !empty( $_POST["message"])){
 
        //3 Les données obligatoires ont-elles une valeur conforme  la forme attendue ?
        if( filter_var($_POST["email"], FILTER_SANITIZE_EMAIL)){
 
            $_POST["email"] = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
 
            if( filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
                $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
 
                if( filter_var($_POST["prenom"], FILTER_SANITIZE_STRING) && filter_var($_POST["nom"], FILTER_SANITIZE_STRING) && filter_var($_POST["telephone"], FILTER_SANITIZE_STRING)) {
                    $_POST["prenom"] = filter_var($_POST["prenom"], FILTER_SANITIZE_STRING);
                    $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                    $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_STRING);
                   
 
                    //4 Les donn�es optionnelles sont-elles pr�sentes ? Les donn�es optionnelles sont-elles remplies ? Les donn�es optionnelles ont-elles une valeur conforme � la forme attendue ?                
                 
 
                    // if( isset($_POST["telephone"])){
                    //     $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_STRING);
                    // }else{$_POST["telephone"] = "";
                    // }
 
                    //5 afficher le mail et le message
                    printf('Bonjour %s, votre message a &eacute;t&eacute; envoy&eacute; et vous serez contact&eacute; dans les plus bref d&eacute;lais !', $_POST["prenom"],'<a href="index.html">Retour à la page d\'accueil</a>');
 
                    //6 utiliser la fonction mail() pour envoyer le message vers botre bo�te mail
                    $to = "cadetyohann@gmail.com";
                    $subject = "Vous avez un nouveau contact";
                    $message = $_POST["prenom"] . ' ' . $_POST["nom"].' vous a écrit<br>'. $_POST["email"];
                    $message .= PHP_EOL . '<br><br>Contenu du message : <br>' . $_POST["message"];
                    if( $_POST["telephone"]){
                        $message .= PHP_EOL . 'Téléphone : ' . $_POST["telephone"]. $_POST["email"];}
                    $headers = 'From: yohann.cadet@labo-ve.fr '. PHP_EOL .
                    'Reply-To: ' .$_POST["email"]. PHP_EOL .
                    'Content-Type: text/html; charset=UTF-8'. PHP_EOL .
                    'X-Mailer: PHP/' . phpversion() ;
                    mail($to, $subject, $message, $headers);
 
 
                }else {$alerte = '<a href="index.html">Il semble que les données que vous avez saisies ne soient pas valides</a>';
                }
 
            }else {$alerte = '<a href="index.html">Votre message contient sans doute des caractéres non appropriés</a>';
            }
 
        }else {$alerte = '<a href="index.html">Merci de saisir une adresse email valide</a>';
        }
 
    }else {$alerte = '<a href="index.html">Merci de remplir les champs obligatoires</a>';
    }
 
}else {$alerte = '<a href="index.html">ok</a>';
}
 
}
?>
<?php if (isset($alerte)) { echo $alerte; } ?>